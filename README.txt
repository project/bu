
CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Installation
 * Configuration
 * Credits


INTRODUCTION
------------
Many internet users are still using very old, out-dated browsers – most of them
for no actual reason. We want to remind these unobtrusively to update their
browser to ensure compatibility with our sites.


INSTALLATION
------------
To install this module, place it in your modules folder and enable it on the
modules page.


CONFIGURATION
------------
This module can be configured from the admin page at

  /admin/config/system/browser-update

Or by browsing to Configuration -> System -> Browser Update Settings


CREDITS
-------
* pebosi https://drupal.org/u/pebosi
* Daniel Moberly https://drupal.org/u/danielmoberly
